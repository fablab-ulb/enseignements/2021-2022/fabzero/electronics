/*
// Telerupt.ino
//
// Will change LED status each time the button is pressed
//
// Nicolas De Coster 2021-03-012
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
// 
*/

#define LED 15
#define BUTTON 8
#define DEBOUNCE 10

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED as an output.
  pinMode(LED, OUTPUT);
  // initialize digital pin BUTTON as an input (with pullup : "high" when unpressed)
  pinMode(BUTTON, INPUT_PULLUP);
  
  //set LED HIGH at startup
  digitalWrite(LED, HIGH);
  
  //Begin serial over USB (no matter the baudrate)
  SerialUSB.begin();
}

// the loop function runs over and over again forever
void loop() {  
    //if button is pressed (signal is low)
    if(!digitalRead(BUTTON)){
      //invert LED output
      digitalWrite(LED, !digitalRead(LED));
      //write a message over USB
      SerialUSB.println("Can't touch this!");
      //debounce button
      delay(DEBOUNCE);
      //trapped in here until button released
      while(!digitalRead(BUTTON)){}
      //debounce button
      delay(DEBOUNCE);
  }
}

# Make your own Arduino board

## General comments 

- This board is based on an [Atmel SAMD11C](https://www.microchip.com/wwwproducts/en/ATSAMD11C14), a 32bits microprocessor integrating USB natively. Arduino UNO is equiped with 8bits microprocessor without USB, called [AtMega328P](https://www.microchip.com/wwwproducts/en/ATMEGA328P)
- [Atmel SAMD11C](https://www.microchip.com/wwwproducts/en/ATSAMD11C14) only has 16kB of program memory (VS 32kB for [AtMega328P](https://www.microchip.com/wwwproducts/en/ATMEGA328P)). Moreover, most of this memory space is required for USB libraries... letting you a small space for your own program.
- [Atmel SAMD11C](https://www.microchip.com/wwwproducts/en/ATSAMD11C14) was chosen because it integrates USB natively, is reasonably easy to solder (SOIC-14 package)

![](https://www.elnec.com/pics/sop-soic/soic14-150m.gif)

## Design

The board was design to simplify the circuit at the very maximum with reasonably easy-to-solder components on a small board.

The board has :
- One [Atmel SAMD11C](https://www.microchip.com/wwwproducts/en/ATSAMD11C14) as core
- One `POWER LED` (red) : always on, connected to power
- One `BUILTIN LED` (green) connected to `PIN 15`
- One `BUTTON`  connected to `PIN 8`
- One `5V-prottected PIN` to `PIN 5`, which is also the `RX` pin of the hardware `SERIAL (~UART)`
- The [bootloader](https://github.com/mattairtech/ArduinoCore-samd/blob/master/bootloaders/zero/binaries/sam_ba_Generic_D11C14A_SAMD11C14A.bin) was already programmed into the chip to ease your work.

- You can program the board through ArduinoIDE thanks to [SAMD CORE for arduino](https://github.com/mattairtech/ArduinoCore-samd), devellopped by MattairTech.

## Settings

Two videos are here to help you setting things in Arduino : 

![](media/08-Add_Board_Arduino.mp4)
![](media/09-Board_Settings.mp4)

See https://github.com/mattairtech/ArduinoCore-samd/blob/master/variants/Generic_D11C14A/README.md documentation.

Add `https://www.mattairtech.com/software/arduino/package_MattairTech_index.json` to the external boards manager, then install it in the board manager.

In Arduino, select :
- `Board` : Generic D11C14A
- `Clock` : INTERNAL_USB_CALIBRATED_OSCILLATOR
- `USB-CONFIG` : CDC_ONLY (or any if you're sure about what you're doing...)
- `SERIAL_CONFIG` : ONE_UART_ONE_WIRE_NO_SPI
- `BOOTLOADER` : 4kB Bootloader (Important! Otherwise your code has no chance to work as code lines will be wrong)
- Leave other parameters to their default (once more : unless you're sure about what you're doing...)

## Pinout

| Arduino | alt | Pin n  |    | Pin n  | alt | Function |
|:-------- | --- | :------ |--- | ------:| --- |--------:|
| 05 | RX | 01 | | 01 | | **5V** ! |
| 08 | Button | 02 | | 02 | | 04 |
| 09 |  | 03 | | 03 | | 02 |
| 14 |  | 04 | | 04 | | 3.3V |
| 15 | LED | 05 | | 05 | | GND |
| 30 |  | 06 | | 06 | | 31 |
| | | | USB CONN| | | |

## Step-by-step

You'll get a pre-milled board

![](media/01.jpeg)

If there is copper remaining at the front of the USB connector, remove it (with a cutter for example)

![](media/02.jpeg)


- **CHECK IC ORIENTATION (TWICE) FIRST!** : the small dot on the microcontroller should be placed at the top left when the board is oriented with USB connector at the bottom. Fix the IC by solder two opposed pins : put a small amount of solder on one pad, solder one pin, then solder the opposite pin.

![](media/00-Fix_IC.mp4)

- Solder the full IC

![](media/01-Solder_IC.mp4)
![](media/02-Solder_IC.mp4)

- The circuit should know look like

![](media/03.jpeg)

- Solder the regulator

![](media/03-Solder_Reg.mp4)

- Solder the 3.3V capacitor (otherwise the regulator will be unstable)

![](media/04-Solder_IC_CAP.mp4)

- Put a little bit of tin on the USB "connector".

![](media/05-USB_Conn.mp4)

![](media/04.jpeg)

- Put a small piece of dubble-tape

![](media/05.jpeg)

- Paste the small plastic adaptor for USB connector

![](media/06.jpeg)

- Time to check that everything's fine so far! Connect it to your computer and check the device is detected. On Linux, use `dmesg -w` command to see it live. On Windows, check your `Peripheral Manager`

![](media/06-Check_USB_recognition.mp4)

- Solder the user LED (green) and its resistor. Check polarity before soldering! (using the mark and/or a multimeter in continuity-check mode)

![](media/07-Solder_LED.mp4)

![](media/07.jpeg)

- Configure ArduinoIDE so you can use SAMD11C.

![](media/08-Add_Board_Arduino.mp4)
![](media/09-Board_Settings.mp4)

- Launch a blink example to check everything's fine with the user LED.

![](media/10-Blink_Example.mp4)

- Solder the button and launch a "Telerupt USB" example to check everyhing is fine with the button.

![](media/11-Telerupt_Example_USB.mp4)

- Solder all the rest, starting by the protection resistor (next to the voltage regulator). Pro tip : always start with the smallest pieces and the pieces located at the center of the board.


![](media/08.jpeg)
![](media/09.jpeg)
